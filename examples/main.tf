provider "libvirt" {
  uri = "qemu:///system"
}

#provider "libvirt" {
#  alias = "server2"
#  uri   = "qemu+ssh://root@192.168.100.10/system"
#}


variable "vm_network_addresses" {
  description = "Defines the network in the CIDR format"
  default = "10.10.100.0/24"
}

variable "vm_network_name" {
  description = "Defines the network name"
  default = "10_10_100_network"
}

# Create a network for our VMs
resource "libvirt_network" "vm_network" {
   name = "${var.vm_network_name}"
   addresses = ["${var.vm_network_addresses}"]
   mode = "bridge"
   bridge = "virbr1"
   dhcp {
       enabled = false
   }

   autostart = true
}