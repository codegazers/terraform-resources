
# Use CloudInit to add our ssh-key to the instance
resource "libvirt_cloudinit_disk" "commoninit-master1" {
          name = "commoninit-master1.iso"
          user_data = "${data.template_file.user_data.rendered}"
          network_config = "${data.template_file.network_config-master1.rendered}"
}


# volume to attach to the "master1" domain as main disk
resource "libvirt_volume" "master1" {
  name           = "master1.qcow2"
  base_volume_id = "${libvirt_volume.base-bionic64.id}"
}


data "template_file" "network_config-master1" {
  template = "${file("${path.module}/network_config-master1.cfg")}"
}


# Create the machine
resource "libvirt_domain" "master1" {
  name = "master1"
  memory = "512"
  vcpu = 1

  cloudinit = "${libvirt_cloudinit_disk.commoninit-master1.id}"

  network_interface {
    network_id = "${libvirt_network.vm_network.id}"
    network_name = "vm_network"
  }

  # IMPORTANT
  # Ubuntu can hang is a isa-serial is not present at boot time.
  # If you find your CPU 100% and never is available this is why
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
      type        = "pty"
      target_type = "virtio"
      target_port = "1"
  }

  disk {
       volume_id = "${libvirt_volume.master1.id}"
  }
  graphics {
    type = "spice"
    listen_type = "address"
    autoport = "true"
  }
}
