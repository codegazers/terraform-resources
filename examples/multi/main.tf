# instance the provider
provider "libvirt" {
  uri = "qemu:///system"
}

# We fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "base-bionic64" {
  name = "base-bionic64"
#  pool = "images" #CHANGE_ME
  source = "https://cloud-images.ubuntu.com/releases/bionic/release/ubuntu-18.04-server-cloudimg-amd64.img"
  format = "qcow2"
}

# Create a network for our VMs
resource "libvirt_network" "vm_network" {
   name = "vm_network"
   addresses = ["10.0.1.0/24"]
   dhcp {
	enabled = true
   }
}

data "template_file" "user_data" {
  template = "${file("${path.module}/user_config.cfg")}"
}
